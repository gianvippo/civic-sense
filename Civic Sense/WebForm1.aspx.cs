﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Civic_Sense
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void UploadFile(object sender, EventArgs e)
        {
            string folderPath = Server.MapPath("~/Files/");

            //Check whether Directory (Folder) exists.
            if (!Directory.Exists(folderPath))
            {
                //If Directory (Folder) does not exists. Create it.
                Directory.CreateDirectory(folderPath);
            }

            //Save the File to the Directory (Folder).
            FileUpload1.SaveAs(folderPath + Path.GetFileName(FileUpload1.FileName));

            //Display the success message.
            lblMessage.Text = Path.GetFileName(FileUpload1.FileName) + " has been uploaded.";
        }

        protected void invia_dati_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|SegnalazioniUtenti.mdf;Integrated Security=True");
            //aggiornamento database
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("insert into segnalazioni values(@email,@description,@lat,@lng,@severity,@status)", con);
                cmd.Parameters.AddWithValue("email", "mammtcafesc@si.it");
                cmd.Parameters.AddWithValue("description", "esploso palazzo");
                cmd.Parameters.AddWithValue("lat", latitude.Value);
                cmd.Parameters.AddWithValue("lng", longitude.Value);
                cmd.Parameters.AddWithValue("severity", severity.SelectedValue);
                cmd.Parameters.AddWithValue("status", "activated");
                cmd.ExecuteNonQuery();
                Response.Write("SI");
            }
        }
    }
}