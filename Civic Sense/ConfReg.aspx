﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfReg.aspx.cs" Inherits="Civic_Sense.ConfReg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <link href="Content/bootstrap.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color">
        <a class="navbar-brand" href="/Welcome.aspx">Civic Sense</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/Login.aspx">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/Registrazione.aspx">Registrati</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/Segnalazione.aspx">Segnalazione</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">Dropdown link
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!--NAVBAR-->

    <!--MESSAGGIO CONFERMA AVVENUTA REG. (DA MODIFICARE?)-->
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <p align="center">
        <img src="http://www.clker.com/cliparts/m/I/n/c/R/q/happy-robot-md.png" alt=" " width="200" height="250" />
    </p>
    <p align="center">
        <span style="color: #FF8000; text-shadow: 0 0 10px #000000; font-size: 37pt;">Congratulazioni, la registrazione è andata a buon fine!
            <br />
            <br />
        </span>
        <span style="color: #FF8000; text-shadow: 0 0 10px #000000; font-size: 30pt;">Clicca su <a href="/Login.aspx">LOGIN</a> per accedere all'area privata e interagire con i nostri servizi!</span>
    </p>
    <!--MESSAGGIO CONFERMA AVVENUTA REG. (DA MODIFICARE?)-->

    <br />
    <p align="center">
        <img src="http://iconshow.me/media/images/ui/ios7-icons/png/512/checkmark-outline.png" alt=" " width="200" heigth="250" />
    </p>

</asp:Content>
