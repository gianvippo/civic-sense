﻿<%@ Page Title="CIVIC SENSE" Language="C#" MasterPageFile="~/MAsterPage.Master" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="Civic_Sense.Welcomee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <link href="Content/bootstrap.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color">
        <a class="navbar-brand" href="/Welcome.aspx">Civic Sense</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/Login.aspx">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/Registrazione.aspx">Registrati</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/Registrazione.aspx">Contatti</a>
                </li>
            </ul>
        </div>
    </nav>
    <!--NAVBAR-->

    <!--TITOLO DI BEVENUTO DEL DIO BOIA-->
    <div align="center">
        <span style="color: #FF8000; text-shadow: 0 0 10px #000000; font-size: 53pt;">Civic Sense</span><br />
    </div>
    <br />
    <br />
    <!--TITOLO DI BEVENUTO-->

    <!--DESCRIZIONE DELL'APPLICAZIONE (FORSE DA ELIMINARE E OTTIMIZZARE)-->
            <p align="center">
                <span style="color: #FFFFFF; text-shadow: 0 0 10px #000000; font-size: 12pt;">
                    Benvenuto in Civic Sense, <br />
                    il portale web per segnalare i problemi urbani che si presentano attorno a te.
    <br />
                    Con l'utilizzo di questo portale potrai navigare <br /> 
                    usufruendo di tutte le funzionalitá disponibili:
    <br />
                    1) Segnalazione di un eventuale problema urbano.
    <br />
                    2) Contattaci per avere piú informazioni possibili: e-mail o facebook dai profili degli admin.
    <br />
                    3) Naviga nel nostro storico per dare un'occhiata alle segnalazioni     <br />
                    effettute da altri utenti sparsi per i comuni italiani.
    <br />
                    4) Che cosa aspetti? Registrati sulla nostra piattaforma per contribuire a migliorare il posto che ti circonda.
    <br />
                    5) Se hai giá effettuato lo registrazione, esegui il login
    <hr width="75%" color="black">
                    <br />
                </span>
            </p>
    <!--DESCRIZIONE DELL'APPLICAZIONE (FORSE DA ELIMINARE E OTTIMIZZARE)-->


    <!--MAPPA GOOGLE-->
    <div align="center">
            <div id="googleMap" style="width: 75%; height: 350px;"></div>

            <script>
                function myMap() {
                    var mapProp = {
                        center: new google.maps.LatLng(41.1137532, 16.8666392),
                        zoom: 11,
                    };
                    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
                }
            </script>

            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0dpAloqHVMJRD5un7GEay6eD7_0gPjZo&callback=myMap"></script>
        <br />
        <br />
        </div>
    </div>
    <!--MAPPA GOOGLE-->

</asp:Content>
