﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Civic_Sense.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" integrity="sha512-07I2e+7D8p6he1SIM+1twR5TIrhUQn9+I6yjqD53JQjFiMf8EtC93ty0/5vJTZGF8aAocvHYNEDJajGdNx1IsQ==" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js" integrity="sha512-A7vV8IFfih/D732iSSKi20u/ooOfj/AGehOKq0f4vLT1Zr2Y+RX7C+w8A1gaSasGtRUZpF/NZgzSAu4/Gc41Lg==" crossorigin=""></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">$USERNAME
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Modifica Profilo</a>
                        <a class="dropdown-item" href="#">Prova 1</a>
                        <a class="dropdown-item" href="#">Prova 2</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Modifica Profilo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/NewReport.aspx">Nuova Segnalazione</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Storico Segnalazioni</a>
                </li>
            </ul>
        </div>
    </nav>
    <!--NAVBAR-->

    <br />
    <br />


    <link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

    

    <div id="mapid" style="width: 600px; height: 400px;"></div>
    <script>

        var mymap = L.map('mapid').setView([41.11, 16.86], 13);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="http://mapbox.com">Mapbox</a>',
            id: 'mapbox.streets'
        }).addTo(mymap);

        //mappa


        var marker = null; //marker

        function onMapClick(e) {

            if (marker != null) {
                mymap.removeLayer(marker);
            }

            marker = new L.marker(e.latlng).addTo(mymap);

            document.getElementById('<%=latitude.ClientID%>').value = e.latlng.lat;
            document.getElementById('<%=longitude.ClientID%>').value = e.latlng.lng;

        }

        mymap.on('click', onMapClick);

       /* function onLocationFound(e) {
            var radius = e.accuracy / 2;
            L.marker(e.latlng).addTo(mymap)
                .bindPopup("You are within " + radius + " meters from this point").openPopup();
            L.circle(e.latlng, radius).addTo(mymap);
        }

        mymap.on('locationfound', onLocationFound);
        mymap.locate({ setView: true, watch: true, maxZoom: 8 });*/

    </script>

   <input id="latitude" type="hidden" runat="server"/>
   <input id="longitude" type="hidden" runat="server"/>


    

    <asp:DropDownList ID="severity" runat="server">
        <asp:ListItem Enabled="true" Text="Verde" Value="1"></asp:ListItem>
        <asp:ListItem Text="Giallo" Value="2"></asp:ListItem>
        <asp:ListItem Text="Rosso" Value="3"></asp:ListItem>
    </asp:DropDownList>

    <asp:TextBox ID="txtMammt" runat="server" CssClass="form-control" Width="270px" Height="36px" ></asp:TextBox>
    

    <asp:Button ID="invia_dati" Text="Invia dati" runat="server" OnClick="invia_dati_Click" />

    <br />
    <br />
    <br />



    <asp:FileUpload ID="FileUpload1" runat="server" />
    <hr width="75%" color="black" align="left">
    <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="UploadFile" />
    <br />
    <asp:Label ID="lblMessage" ForeColor="Green" runat="server" />



</asp:Content>
