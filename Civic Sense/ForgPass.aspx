﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ForgPass.aspx.cs" Inherits="Civic_Sense.ForgPass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet"/>
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <link href="Content/bootstrap.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color">
        <a class="navbar-brand" href="/Welcome.aspx">Civic Sense</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/Login.aspx">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/Registrazione.aspx">Registrati</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">Dropdown link
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!--NAVBAR-->


    <br /><br /><br /><br />
    <div align="center">
        <span style="color: #FF8000; text-shadow: 0 0 10px #000000; font-size: 20pt;">Per resettare la password, compila i campi qui sotto</span>
        <hr width="75%" color="black"><br />
    </div>

    <!-- FORM PASSWORD DIMENTICATA -->
    <br />
<div align="center">
<form>
    <!-- E-MAIL -->
    <div class="md-form form-group mt-5" style="width: 38rem;">
        <label for="formGroupExampleInputMD">E-Mail</label>
        <input type="text" class="form-control" id="formGroupExampleInputMD">
    </div>
    <!-- NUOVA PASSWORD -->
    <div class="md-form form-group mt-5" style="width: 38rem;">
        <label for="formGroupExampleInput2MD">Nuova password</label>
        <input type="text" class="form-control" id="formGroupExampleInput2MD">
    </div>
    <!-- CONFERMA NUOVA PASSWORD -->
    <div class="md-form form-group mt-5" style="width: 38rem;">
        <label for="formGroupExampleInput2MD">Conferma nuova password</label>
        <input type="text" class="form-control" id="formGroupExampleInput2MD">
    </div>
</form>
    <!-- FORM PASSWORD DIMENTICATA -->

</div>

</asp:Content>
