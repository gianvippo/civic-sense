﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Civic_Sense
{
    public partial class Registrazione : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Button_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection (@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|SignUp.mdf;Integrated Security=True");
            //aggiornamento database
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("insert into reg values(@name,@surname,@email,@password,@city,@cap,@address)",con);
                cmd.Parameters.AddWithValue("name", txtNome.Text);
                cmd.Parameters.AddWithValue("surname", txtCognome.Text);
                cmd.Parameters.AddWithValue("email", txtEmail.Text);
                cmd.Parameters.AddWithValue("password", txtPassword.Text);
                cmd.Parameters.AddWithValue("city", txtCittà.Text);
                cmd.Parameters.AddWithValue("cap", txtCap.Text);
                cmd.Parameters.AddWithValue("address", txtIndirizzo.Text);
                cmd.ExecuteNonQuery();
                Response.Redirect("/Confreg.aspx");
            }

        
        }
    }
}
