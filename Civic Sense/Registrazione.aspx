﻿<%@ Page Title="REGISTRATI" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Registrazione.aspx.cs" Inherits="Civic_Sense.Registrazione" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <link href="Content/bootstrap.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color">
        <a class="navbar-brand" href="/Welcome.aspx">Civic Sense</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/Login.aspx">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/Registrazione.aspx">Registrati</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">Dropdown link
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!--NAVBAR-->

    <br />
    <p align="center">
        <span style="color: #FF8000; text-shadow: 0 0 10px #000000; font-size: 45pt;">Registrazione</span>
    </p>
    <br />

    <!-- NOME-->
    <br />
    <p style="margin-left: 625px;">
        Nome
    </p>
    <div align="center">
        <asp:TextBox ID="txtNome" runat="server" CssClass="form-control" Width="270px" Height="36px" /><br>
        <asp:RequiredFieldValidator
            runat="server" ControlToValidate="txtNome"
            ErrorMessage="*Inserire un nominativo"
            ForeColor="Red">
        </asp:RequiredFieldValidator>
    </div>
    <br />

    <!-- COGNOME -->
    <p style="margin-left: 625px;">
        Cognome
    </p>
    <div align="center">
        <asp:TextBox ID="txtCognome" runat="server" CssClass="form-control" Width="270px" Height="36px" /><br>
        <asp:RequiredFieldValidator
            runat="server" ControlToValidate="txtCognome"
            ErrorMessage="*Inserire un cognome"
            ForeColor="Red"> 
        </asp:RequiredFieldValidator>
    </div>
    <br />

    <!--E-MAIL-->
    <p style="margin-left: 625px;">
        E-Mail
    </p>
    <div align="center">
        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" Width="270px" Height="36px" /><br>
        <asp:RequiredFieldValidator
            runat="server" ControlToValidate="txtEmail"
            ErrorMessage="*Inserire un'email"
            ForeColor="Red"> 
        </asp:RequiredFieldValidator>
    </div>
    <br />
    
    <!--PASSWORD-->
    <p style="margin-left: 625px;">
        Password
    </p>
    <div align="center">
        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" Width="270px" Height="36px" /><br>
        <asp:RegularExpressionValidator
            runat="server"
            ControlToValidate="txtPassword"
            ValidationExpression="^[0-9]*$"
            ErrorMessage="*Inserire almeno una cifra" ForeColor="Red">      
        </asp:RegularExpressionValidator>

    </div>
    <br />

    <!--CITTA' RESIDENZA-->
    <p style="margin-left: 625px;">
        Città di residenza
    </p>
    <div align="center">
        <asp:TextBox ID="txtCittà" runat="server" CssClass="form-control" Width="270px" Height="36px" /><br>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="txtCittà" ErrorMessage="*Inserire una città di residenza"
            ForeColor="Red">
        </asp:RequiredFieldValidator>
    </div>
    <br />

    <!--CAP-->
    <p style="margin-left: 625px;">
        CAP
    </p>
    <div align="center">
        <asp:TextBox ID="txtCap" runat="server" CssClass="form-control" Width="270px" Height="36px" /><br>
        <asp:RegularExpressionValidator
            runat="server"
            ControlToValidate="txtCap"
            ValidationExpression="^[0-9]*$"
            ErrorMessage="*Inserire solo cifre per il CAP" ForeColor="Red">              
        </asp:RegularExpressionValidator>
    </div>
    <br />

    <!--INDIRIZZO DI RESIDENZA-->
    <p style="margin-left: 625px;">
        Indirizzo di Residenza
    </p>
    <div align="center">
        <asp:TextBox ID="txtIndirizzo" runat="server" CssClass="form-control" Width="270px" Height="36px" /><br>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtIndirizzo"
            ErrorMessage="*Inserire un indirizzo di residenza" ForeColor="Red">
        </asp:RequiredFieldValidator>
    </div>
    <br />

    <!-- INVIA DATI-->
    <br />
    <div align="center">
        <asp:Button ID="Button" runat="server" CssClass="btn btn-danger btn-block z-depth-2" Width="270px" Height="36px" OnClick="Button_Click" Text="Registrati" Font-Size="15px" />
    </div>
    <td colspan="2" style="color: red">
        <asp:Label ID="lbltxt" runat="server" /></td>

    <br>
    <br>
</asp:Content>
