﻿<%@ Page Title="LOGIN" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Civic_Sense.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <link href="Content/bootstrap.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color">
        <a class="navbar-brand" href="http://localhost:3350/Welcome.aspx">Civic Sense</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost:3350/Login.aspx">Contatti</a>
                </li>
            </ul>
        </div>
    </nav>
    <!--NAVBAR-->

    <!--FORM DI LOGIN-->
    <div align="center">
        <section class="form-simple">

            <div align="left" style="width: 26rem;">

                <div class="header pt-3 grey lighten-2">

                    <br />
                    <div align="center">
                        <div class="row d-flex justify-content-start">
                            <span style="color: #FF8000; text-shadow: 0 0 10px #000000; font-size: 30pt;">Login</span>
                        </div>
                    </div>
                </div>

                <div class="card-body mx-4 mt-4">

                    <!--E-MAIL-->
                    <p align="left">
                        E-Mail
                    </p>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"/><br>
                    <asp:RequiredFieldValidator
                        runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="*Inserire un'email"
                        ForeColor="Red"> 
                    </asp:RequiredFieldValidator>
                    

                    <!--PASSWORD-->
                    <p align="left">
                        Password
                    </p>
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"/><br>
                    <asp:RegularExpressionValidator
                        runat="server"
                        ControlToValidate="txtPassword"
                        ValidationExpression="^[0-9]*$"
                        ErrorMessage="*Inserire almeno una cifra" ForeColor="Red">              
                    </asp:RegularExpressionValidator>                           
                    <p class="font-small grey-text d-flex justify-content-end">
                        Password
            <a href="http://localhost:3350/ForgPass.aspx" class="dark-grey-text font-weight-bold ml-1">dimenticata?</a><br />
                    </p>
                </div>


                <div class="text-center mb-4">
                <p style="margin-left: 44px;">
                    <asp:Button ID="Button1" runat="server" Text="Log In" Text-align="center" CssClass="btn btn-danger btn-block z-depth-2" Width="330px" OnClick="Login_Click" /></p>
                </div><br /><br />
                <p class="font-small grey-text d-flex justify-content-center">
                    Non hai un account? 
                <a href="http://localhost:3350/Registrazione.aspx" class="dark-grey-text font-weight-bold ml-1">Registrati adesso!</a>
                </p>

            </div>
    </div>
    <!--FORM DI LOGIN-->
    
    <!--MESSAGGIO DI LOGIN FALLITO-->
    <br /><br />
    <div align="center">
        <asp:Label ID="LabelLoginFail"
           style="color:#FF0000" 
           Font-Size="25px"
           runat="server">
          </asp:Label>
    </div>
    <!--MESSAGGIO DI LOGIN FALLITO-->

</asp:Content>
