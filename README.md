# Civic Sense



## Cos'è

Civic Sense è una piattaforma online che permette a un cittadino di segnalare agevolmente guasti, problemi, malfunzionamenti e, in generale, «eventi rilevanti» per un soggetto che eroga servizi o gestisce infrastrutture di interesse pubblico (Acquedotto, elettricità, strade e viabilità, sicurezza urbana, ecc…).
